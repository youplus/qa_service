import json
from tastypie.resources import ModelResource, Resource
from tastypie.authorization import Authorization
from django.db import models
from django.http import HttpResponse
from tastypie import fields
from api.models import Note
from controllers import questions_n_snippets

import pdb


class QAResourceObject(object):
    """
    Convert dictionary to object
    @source http://stackoverflow.com/a/1305561/383912
    """

    def __init__(self, d):
        self.__dict__.update(d)

    def __getattr__(self, key):
        return json.loads(json.dumps(key), object_hook=QAResourceObject)


class QAResource(Resource):
    success = fields.BooleanField(attribute='success')
    message = fields.CharField(attribute='message')
    userId = fields.CharField(attribute='userId')
    siteId = fields.CharField(attribute='siteId')
    widgetId = fields.CharField(attribute='widgetId')

    class Meta:
        resource_name = 'qa_service'

    def obj_get_list(self, bundle=None, **kwargs):
        user_id = bundle.request.GET['userId']
        site_id = bundle.request.GET['siteId']
        widget_id = bundle.request.GET['widgetId']

        response = []
        status = questions_n_snippets.api_map_question_to_snippets(user_id, site_id, widget_id)
        
        if status == True:
            response.append(QAResourceObject(
                {
                    'success': True,
                    'message': 'QnA model has been Initiated. Please check back in 1 hour',
                    'userId': user_id,
                    'siteId': site_id,
                    'widgetId': widget_id
                }
            ))
        else:
            response.append(QAResourceObject(
                {
                    'success': False,
                    'message': 'Assigning cbm went wrong.',
                    'userId': user_id,
                    'siteId': site_id,
                    'widgetId': widget_id
                }
            ))
        return response
