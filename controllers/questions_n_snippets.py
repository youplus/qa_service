from utilities import cbm, view_page, engine, qa_service_queue
import pdb
from qa_service import log

logger = log.Logger()
log = logger.get_logger()

def map_question_to_snippets():
    log.info('map_question_to_snippets')
    #page_info = cbm.get_cbm()
    #pdb.set_trace()
    #page_info = view_page.get_page_info() #Getting page info from mongo_user.sites.widget_config object
    page_info = qa_service_queue.get_page()
    log.info('Got '+str(len(page_info))+' pages with corrected cbm')
    log.info('====================================================')
    for idx, page in enumerate(page_info):
        #pdb.set_trace()
                                              ###later can modify the query/ function to get questions to specific C & B & M. 
        # ques = cbm.get_c_questions(page['c']) ###For Now, taking question from only matching category
        # ques = [q['ques'] for q in ques] #For Now sending list of questions(["ques1", "ques2"]) to QandA Model, In future we may want to send list of objects([{'ques':'ques1','created_at':'date'}])
        # ###request structure for qa model:
        # ### {"c": "", "b": "", "m": "", "questions":[]}
        # model_req_params = {"c": page['c'], "b": page['b'], "m": page['m'], "questions":ques}
        engine.map_ques_and_snippets(page)
    return None

def re_map_question_to_snippets():
    log.info('re_map_question_to_snippets')
    #page_info = cbm.get_cbm()
    #pdb.set_trace()
    page_info = view_page.get_less_videos_page()
    log.info('Got '+str(len(page_info))+' pages with corrected cbm and have found less videos when it ran 1st time.')
    log.info('====================================================')
    for idx, page in enumerate(page_info):
        #pdb.set_trace()
                                              ###later can modify the query/ function to get questions to specific C & B & M. 
        # ques = cbm.get_c_questions(page['c']) ###For Now, taking question from only matching category
        # ques = [q['ques'] for q in ques] #For Now sending list of questions(["ques1", "ques2"]) to QandA Model, In future we may want to send list of objects([{'ques':'ques1','created_at':'date'}])
        # ###request structure for qa model:
        # ### {"c": "", "b": "", "m": "", "questions":[]}
        # model_req_params = {"c": page['c'], "b": page['b'], "m": page['m'], "questions":ques}
        engine.map_ques_and_snippets(page)
    return None


def api_map_question_to_snippets(user_id, site_id, widget_id):
    try:
        log.info('-------------------------API-----------------------')
        log.info('api_map_question_to_snippets')
        page = view_page.get_page(user_id, site_id, widget_id)
        log.info('Got page: '+str(len(page)))
        qa_service_queue.create(page[0])
        log.info('====================================================')
        return True
    except Exception as e:
        log.error(e)