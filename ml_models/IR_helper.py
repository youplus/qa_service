# -*- coding: utf-8 -*-
__author__ = "Naval Chand"
__version__ = 'v0.1'

import string
import re

exclude = set(string.punctuation)
# exclude.append('\x')
regex = re.compile('[%s]' % re.escape(string.punctuation))

def remove_puntuations(s):
    return re.sub('[^A-Za-z0-9+]+', ' ', s)

def basic_preprocess(string):
    try:
        ### convert to lower case
        string = string.lower()
        ### remove puntuations
        string = remove_puntuations(string)
        return string.strip()
    except:
        return ""



def seprate_suffix(word):
    counter = 0
    upper_check = True
    for i in reversed(word):
        if i.isupper() and upper_check:
#             print i
            counter +=1
        else:
            upper_check = False
    suffix = word[-counter:]
    prefix = word[:-counter]
    
    return [prefix,suffix]
    
def get_suffix(string):
    return " ".join([" ".join(seprate_suffix(w)) if w[-1].isupper() else w for w in string.split()]).strip()

### https://gist.github.com/bgusach/a967e0587d6e01e889fd1d776c5f3729
def convert_no_str(string):
#     print string
    """
    Given a string and a replacement map, it returns the replaced string.
    :param str string: string to execute replacements on
    :param dict replacements: replacement dictionary {value to find: value to replace}
    :rtype: str
    """
    
    replacements = {"0": " zero ", 
       "1": " one ",
      "2" : " two ",
      "3" : " three ",
      "4" : " four ",
      "5" : " five ",
      "6" : " six ",
      "7" : " seven ",
      "8" : " eight ",
      "9" : " nine ",
      "+" : "plus"}


    # Place longer ones first to keep shorter substrings from matching where the longer ones should take place
    # For instance given the replacements {'ab': 'AB', 'abc': 'ABC'} against the string 'hey abc', it should produce
    # 'hey ABC' and not 'hey ABc'
    substrs = sorted(replacements, key=len, reverse=True)

    # Create a big OR regex that matches any of the substrings to replace
    regexp = re.compile('|'.join(map(re.escape, substrs)))
    # For each match, look up the new string in the replacements
    return regexp.sub(lambda match: replacements[match.group(0)], string).strip()
    
def get_search_term(model, brand):
    terms = []
    terms.append(model.lower())
    if (len(model.split(" ")) > 1) and (brand.lower() in model.lower()):
        ### check for brand present in the search term
        ### removing it for the search term and appending it to the terms list
        ### Ex: Brand--> Apple,  Model--> Apple iPhone X
        terms.append(model.lower().replace(brand.lower(),"").strip())

    elif (brand.lower() not in model.lower()):
        terms.append(" ".join([w for w in (brand + " " + model).split(" ") if w]))
    return terms

def adv_str_preprocess(string):
    try:
        string = convert_no_str(string)
        return basic_preprocess(string)
    except:
        return ""

def adv_suffix_preprocess(string):
    try:
        string = get_suffix(string)
        return basic_preprocess(string)
    except:
        return ""


def get_terms(term , brand):
    terms_list = []
    preprocess_list = []

    terms_list = []

    ### check for suffix replacement
    suffix_term = get_suffix(term)
    if term != suffix_term:
        #terms_list.append(suffix_term)
        temp_terms = get_search_term(suffix_term, brand)
        terms_list.extend(zip(temp_terms , [2]*len(temp_terms)))

    term = basic_preprocess(term)
    brand = basic_preprocess(brand)
    ### RAW word
    #terms_list.append(term)

    ### check for brand present in model
    temp_terms = get_search_term(term, brand)
    terms_list.extend(zip(temp_terms,[0]*len(temp_terms)))

    ### check for numaric replacement
    num_term = convert_no_str(term)
    if term != num_term:
        #terms_list.append(num_term)
        temp_terms = get_search_term(num_term, brand)

        terms_list.extend(zip(temp_terms , [1]*len(temp_terms)))




#     print "Terms Found for {},are {}".format(CBM_dummy_data[search_term_index]['model'], terms_list)
    
    return list(set([w[0] for w in terms_list]))


def validate_term(model,brand):
    if brand.lower() not in model.lower():
        return brand.strip() + " " + model.strip()
    else:
        return model.strip()

