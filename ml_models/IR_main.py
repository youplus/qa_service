#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" industry.py : Loads model corresponding to the industry """
__author__ = 'Naval'
__contributers__ = ['Sumanth','Dharmanath']
__copyright__ = 'Copyright 2018, Question & Answer'
__version__ = '1.0.1'
__status__ = 'Production'
__date__ = 'Fri Jan 11 12:22:40 IST 2019'


import pandas as pd
import numpy as np
import string
import re
from elasticsearch import Elasticsearch as ES
from IR_helper import *
import json
from question_answer import rank as L3_rank
from question_answer import Snippet
import sys
import os
from bson.objectid import ObjectId

from qa_service import log
logger = log.Logger()
log = logger.get_logger()


class IR(object):
	def __init__(self, elastic_ip = "https://elasticsearch.yupl.us",
						L1_index = "youplus_q_tags",
						L2_index = "youplus_q_tags",
						no_of_parent_ids=15,
						max_es_records=1000,
						max_snippets = False,
						):
		""" 
		Args: None
		Returns: None
		Function : Initializes all the models
		"""

		self.elastic_ip = elastic_ip
		self.L1_index = L1_index
		self.L2_index = L2_index
		self.es = ES([self.elastic_ip])
		self.no_of_parent_ids = no_of_parent_ids
		self.max_es_records = max_es_records
		self.max_snippets = max_snippets
		self.lower_bound = 15.0
		self.upper_bound = 60.0


	def predict(self, CBM_obj, questions):
		""" 
		Args: parent_ids : Parent Ids
			  question : Question
		Returns: Elastic Search Query for L2
		Function : Returns Elastic Search Query for L2
		"""
		try:
			C = CBM_obj['C']
			B = CBM_obj['B']
			M = CBM_obj['M']
			I = CBM_obj['I']
			######## 1st Version #############
			########## Catagory for CBM model at page level and transripit level are different
			# terms = get_terms(M , B)
			# terms = list(set([" ".join(w.split()) for w in terms]))
			########## 1st version ########

			######## 2nd version #############
			word = validate_term(M, B)
			terms = get_terms(word , "")
			terms = list(set([" ".join(w.split()) for w in terms]))
    		######## 2nd version #############



			##### L1
			log.info("**L1: entering for C:"+str(C)+"  B:"+str(B)+"  M:"+str(M)+" **")
			body = self.L1_query(terms , C)
			res = self.es.search(index=self.L1_index, body=body, size=self.max_es_records)
			if len(res['hits']['hits']) > 0:
				data_df = pd.DataFrame([[w['_score'],w['_id'],
				w['_source']['title'] if 'title' in w['_source'] else 'NOT FOUND',
				w['_source']["transcribed_text"] if 'transcribed_text' in w['_source'] else 'NOT FOUND',
				w['_source']['video_url'] if 'video_url' in w['_source'] else 'NOT FOUND',
				w['_source']["duration"] if "duration" in w['_source'] else "NOT FOUND",
				w['_source']['parent_video_id'] if 'parent_video_id' in w['_source'] else 'NOT FOUND'] for w in res['hits']['hits']],
                columns = ['_score' , "_id", "title","transcribed_text",'video_url',"duration" , 'parent_video_id'])
				# data_df = pd.DataFrame([[w['_score'],w['_id'],
				# 			 w['_source']['title'] if 'title' in w['_source'] else 'NOT FOUND',
				# 			 w['_source']['video_url'] if 'video_url' in w['_source'] else 'NOT FOUND',
				# 			 w['_source']['parent_video_id'] if 'parent_video_id' in w['_source'] else 'NOT FOUND'] for w in res['hits']['hits']],
				# 								   columns = ['_score' , "_id", "title",'video_url' , 'parent_video_id'])
				### remove records where parent id is not present
				data_df = data_df[data_df['parent_video_id'] != 'NOT FOUND']
				### get parent ids based on the scores
				parent_ids = data_df['parent_video_id'].tolist()
				parent_ids = [id_ for id_ in parent_ids if id_!=None]
				log.info("L1: Number of parent ids indentified from L1:"+str(parent_ids))
				log.info("**L1: exiting for C:"+str(C)+"  B:"+str(B)+"  M:"+str(M)+" **")
				
				response = {}
				####### L2
				l3_data = list()
				log.info("**L2: entering for C:"+str(C)+"  B:"+str(B)+"  M:"+str(M)+" **")
				for question in questions:
					try:
						log.info("^^L2: entering for question ^^"+str(question))
						body = self.L2_query(parent_ids , question)
						res = self.es.search(index=self.L2_index, body=body, size=self.max_es_records)
					except Exception as identifier:
						continue
					
					if len(res['hits']['hits']) > 0:
						log.info("L2: got snippets for the question: "+str(question))
						snp_data_df =pd.DataFrame([[w['_score'],w['_id'],
						w['_source']['title'] if 'title' in w['_source'] else 'NOT FOUND',
						w['_source']["transcribed_text"] if 'transcribed_text' in w['_source'] else 'NOT FOUND',
						w['_source']['video_url'] if 'video_url' in w['_source'] else 'NOT FOUND',
						w['_source']["duration"] if "duration" in w['_source'] else "NOT FOUND",
						w['_source']['parent_video_id'] if 'parent_video_id' in w['_source'] else 'NOT FOUND'] for w in res['hits']['hits']],
						columns = ['_score' , "_id", "title","transcribed_text",'video_url',"duration" , 'parent_video_id'])


						# snp_data_df = pd.DataFrame([[w['_score'],w['_id'],
						# 				 w['_source']['title'] if 'title' in w['_source'] else 'NOT FOUND',
						# 											 w['_source']['transcribed_text'] if 'transcribed_text' in w['_source'] else 'NOT FOUND',
						# 				 w['_source']['parent_video_id'] if 'parent_video_id' in w['_source'] else 'NOT FOUND'] for w in res['hits']['hits']],
						# 									   columns = ['_score' , "_id", "title" , 'transcribed_text','parent_video_id'])

						### remove records where parent id is not present
						snp_data_df = snp_data_df[snp_data_df['parent_video_id'] != 'NOT FOUND']

						##### combining results of L1 and L2
						merged_data_df = pd.merge(snp_data_df, data_df, left_on='parent_video_id', right_on='parent_video_id', how='left')
						merged_data_df['final_score'] = merged_data_df['_score_x'] * merged_data_df['_score_y']
						# final_df = merged_data_df[['_id_x','parent_video_id','title_x','transcribed_text','final_score']].sort_values('final_score', ascending=False)
						final_df = merged_data_df.sort_values('final_score', ascending=False)

						###### L3
						
						if self.max_snippets:
							final_df = final_df[:self.max_snippets]
						else:
							final_df = final_df[:20]

						# final_df['type_'] = final_df['duration_x'].apply(lambda x: type(x).__name__) 
						# final_df = final_df[final_df['type_']=="float"]
						# final_df = final_df[final_df['duration_x'] >= self.lower_bound]
						# final_df = final_df[final_df['duration_x'] <= self.upper_bound]

						final_df['duration'] = final_df['video_url_x'].apply(self.get_duration)
						#print final_df.iloc[:]['duration']
						#final_df = final_df[final_df['duration']!="NOT FOUND"]
						final_df = final_df[final_df['duration'] >= self.lower_bound]
						final_df = final_df[final_df['duration'] <= self.upper_bound]
						#print final_df.iloc[:]['duration']
						log.info("L2: Number of snippets after filteration "+str(len(final_df)))

						
						if len(final_df)!=0:
							snippet_data = [Snippet(s_id,snip,x,y,comb) for (s_id,snip,x,y,comb) in final_df[['_id_x','transcribed_text_x','_score_x','_score_y','final_score']].values.tolist()]
							l3_data.append((question,snippet_data,I))
							log.info("^^L2 completed for question ^^"+str(question))
					else:
						log.info("L2: dint't get snippets for the question: "+str(question)+" ----")
						#print "l2 dint"
				log.info("**L2: exiting for C:"+str(C)+"  B:"+str(B)+"  M:"+str(M)+" **")
				log.info("**L3: entering for C:"+str(C)+"  B:"+str(B)+"  M:"+str(M)+" **")
				qa_response = L3_rank(l3_data)
				

				if qa_response == None:
					log.info("L3: exception")
					raise Exception("exception")

				for res_ in qa_response:
					if len(res_)==3:
						#print "someting went wrong with question",res_[0]
						log.info("L3: Something went wrong with question"+str(res_[0])+" "+str(res_[1]))
					else:

						question__ =  res_[0]
						op_snippet_data = res_[1]

						if len(op_snippet_data)!=0:
							log.info("L3: found snippets for (>TH) : ("+str(len(op_snippet_data))+") :"+str(question__))
						else:
							log.info("L3: din't find snippets for (>TH)"+str(question__))
						
						for snippet in op_snippet_data:
							res_obj = { "qs_id":ObjectId(),
										"question":question__,
										"q_tags_id":ObjectId(snippet.get_snippet_id()),
										"qs_c": C ,
										"qs_b": B,
										"qs_m": M,
										"qs_i": I,
										"qs_is_corrected": False,
										"qs_is_selected": False,
										"qs_flat_score_list":snippet.get_flat_score_list(),
										"qs_normalized_score_list":snippet.get_norm_score_list(),
										"qs_pairwise_score_list":snippet.get_pairwise_score_list(),
										"qs_termToterm_score_list":snippet.get_termToterm_score_list(),
										"qs_bilinear_score_list":snippet.get_bilinear_score_list(),
										"qs_max_flat_score":snippet.get_max_flat_score(),
										"qs_max_norm_score":snippet.get_max_norm_score(),
										"qs_L1":snippet.get_l1(),
										"qs_L2":snippet.get_l2(),
										"qs_L1_L2_combined":snippet.get_l1_l2()
										}
							if snippet.get_snippet_id() in response:
								response[snippet.get_snippet_id()].append(res_obj)
							else:
								response[snippet.get_snippet_id()] = [res_obj]
				log.info("**L3: exiting for C:"+str(C)+"  B:"+str(B)+"  M:"+str(M)+" **")
				return {"status":True, "data" : response, "message":"done", "error":""}
			else:
				return {"status":True,"data":{}, "message":"less_videos", "error":""}
		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			log.info("Exception in model main file.")
			log.info(e)
			#print exc_type, fname, exc_tb.tb_lineno
			return {"status":False, "data":{}, "message": "model_failed", "error":e}
			
	def L2_query(self, parent_ids, question):
		""" 
		Args: parent_ids : Parent Ids
			  question : Question
		Returns: Elastic Search Query for L2
		Function : Returns Elastic Search Query for L2
		"""
		snp_query = {
				  "query": {
					"bool": {
					  "must": {
						"multi_match": {
						  "query": question,
						  "fields": [
							"transcribed_text"
						  ]
						}
					  },
					  "filter": [
						{
						  "term": {
							"is_parent_video": False
						  }
						},
		
#                           {
#                           "term": {
#                             "is_view": True
#                           }
#                         },
						{
						  "terms": {
							"parent_video_id": parent_ids
						  }
						}
					  ]
					}
				  }
				}
	
		return snp_query

	def L1_query(self, terms , category):
	    query_text = " OR ".join([" AND ".join( ("(" + w.strip() + ")").split(" ")) for w in terms])
	    body = {
	           "query":{
	              "bool":{
	                 "must":{
	                    "query_string":{
	#                        "fields" : ["title^5", "transcribed_text^2","caption"],
	                        "fields" : ["title"],
	                       "query" : query_text,
	                       "tie_breaker" : 0
	                    }
	                 },
	                 "filter":{
	                    "term":{
	                       "is_parent_video": True
	                    }
	                 }
	              }
	           }
	        }
	    return body

	def get_duration(self, url):
		try:
			time_list = url.split("=")[-1].split(",")
			return float(time_list[-1]) - float(time_list[-2])
		except Exception as e:
			return 0.0

	############ 1st Version #################

	# def L1_query(self, terms, category):
	# 	""" 
	# 	Args: terms : Multiple search terms to query parent videos
	# 		  category : Industry Category to filter relavent parent videos
	# 	Returns: Elastic Search Query for L1
	# 	Function : Returns Elastic Search Query for L1
	# 	"""
	# 	query_text = " OR ".join([" AND ".join( ("(" + w.strip() + ")").split(" ")) for w in terms])
	# 	body = {
	# 		  "query": {
	# 			"function_score": {
	# 			  "query": {
	# 				"bool": {
	# 				  "must": {
	# 					"multi_match": {
	# 					  "query": query_text,
	#                         # "operator": "and",
	# 					  "fields": [
	# 						"title",
	# 						"caption",
	# 						"transcribed_text"
	# 					  ]
	# 					}
	# 				  },
	# 				  "filter": [
	# 					{
	# 					  "term": {
	# 						"is_parent_video": True
	# 					  }
	# 					},
	# 					{
	# 					  "term": {
	# 						"industry_category": category
	# 					  }
	# 					}
	# 				  ]
	# 				}
	# 			  },
	# 			  "boost": "5",
	# 			  "functions": [
	# 				{
	# 				  "filter": {
	# 					"match": {
	# 					  "title": {
	# 						"query": query_text,
	# 						"operator": "and"
	# 					  }
	# 					}
	# 				  },
	# 				  "weight": 25
	# 				},
					
	# 				{
	# 				  "filter": {
	# 					"match": {
	# 					  "caption": {
	# 						"query": query_text,
	# 						"operator": "and"
	# 					  }
	# 					}
	# 				  },
	# 				  "weight": 3
	# 				},
	# 				{
	# 				  "filter": {
	# 					"match": {
	# 					  "transcribed_text": {
	# 						"query": query_text,
	# 						"operator": "and"
	# 					  }
	# 					}
	# 				  },
	# 				  "weight": 6
	# 				}
	# 			  ],
	# 			  "score_mode": "max",
	# 			  "boost_mode": "multiply",
	# 			}
	# 		  }
	# 		}
	# 	return body
	############ 1st Version #################

	############ 2st Version #################
	# def L1_query(self, terms, category):
	# 	""" 
	# 	Args: terms : Multiple search terms to query parent videos
	# 		  category : Industry Category to filter relavent parent videos
	# 	Returns: Elastic Search Query for L1
	# 	Function : Returns Elastic Search Query for L1
	# 	"""
	# 	query_text = " OR ".join([" AND ".join( ("(" + w.strip() + ")").split(" ")) for w in terms])
	# 	body = {
	# 		  "query": {
	# 			"function_score": {
	# 			  "query": {
	# 				"bool": {
	# 				  "must": {
	# 					"multi_match": {
	# 					  "query": query_text,
	# #                         "operator": "and",
	# 					  "fields": [
	# 						"title",
	# 						"caption",
	# 						"transcribed_text"
	# 					  ]
	# 					}
	# 				  },
	# 				  "filter": [
	# 					{
	# 					  "term": {
	# 						"is_parent_video": True
	# 					  }
	# 					},
	# 					{
	# 					  "term": {
	# 						"industry_category": category
	# 					  }
	# 					}
	# 				  ]
	# 				}
	# 			  },
	# 			  "boost": "5",
	# 			  "functions": [
	# 				{
	# 				  "filter": {
	# 					"match": {
	# 					  "title": {
	# 						"query": query_text,
	# 						"operator": "and"
	# 					  }
	# 					}
	# 				  },
	# 				  "random_score": {},
	# 				  "weight": 20
	# 				},
	# 				{
	# 				  "filter": {
	# 					"match": {
	# 					  "search_term": {
	# 						"query": query_text,
	# 						"operator": "and"
	# 					  }
	# 					}
	# 				  },
	# 				  "weight": 5
	# 				},
	# 				{
	# 				  "filter": {
	# 					"match": {
	# 					  "caption": {
	# 						"query": query_text,
	# 						"operator": "and"
	# 					  }
	# 					}
	# 				  },
	# 				  "weight": 5
	# 				},
	# 				{
	# 				  "filter": {
	# 					"match": {
	# 					  "transcribed_text": {
	# 						"query": query_text,
	# 						"operator": "and"
	# 					  }
	# 					}
	# 				  },
	# 				  "weight": 5
	# 				}
	# 			  ],
	# 			  "max_boost": 42,
	# 			  "score_mode": "max",
	# 			  "boost_mode": "multiply",
	# 			  "min_score": 42
	# 			}
	# 		  }
	# 		}
	# 	return body
	############ 2st Version #################
