#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" industry.py : Loads model corresponding to the industry """
__author__ = 'Dharmanath'
__contributers__ = ['Sumanth','Naval']
__copyright__ = 'Copyright 2018, Question & Answer'
__version__ = '1.0.1'
__status__ = 'Production'
__date__ = 'Fri Jan 11 12:22:40 IST 2019'

from IR_main import IR
from IR_helper import *
import pandas as pd
from qa_service.config import config


#ir = IR(elastic_ip="https://elasticsearch.yupl.us", L1_index = "youplus_q_tags", L2_index = "youplus_q_tags", max_snippets=200)
ir = IR(elastic_ip=config.configuration['ELASTIC_IP'], L1_index = config.configuration['L1_index'], L2_index = config.configuration['L2_index'], max_snippets=200)

# cbm_obj = {'C':'guitars',
#            'B' : 'Fender',
#            'M':'squire',
#            'industry_code': 1001,
#            'I': 'Musical Instruments'}
# questions = [
#   "does it stay in tune ?",
#   "does it have any tuning problem ?",
#   "how is the design of this guitar ?",
#   "is it expensive to buy ?",
#   "what is the price of the guitar ?",
#   "what is the material used for guitar ?"
#   ]
# ir.predict(cbm_obj,questions)

# cbm_obj = {'C':'Mobiles',
#            'B' : 'Apple',
#            'M':'iphone 8',
#            'industry_code': 1001,
#            'I': 'Cell Phones'}
# questions = [
#   "Does the camera include portrait mode ?",
#   "Does the camera have image stabilization ?",
#   "What is the slow motion rate of the camera ?",
#   "Is front facing camera good enough ?",
#   "How fast does the phone charge ?",
#   "Is the phone water resistant ?",
#   "Does this phone has fingerprint scanner ?",
#   "How do you feel about wireless charger ?",
#   "Does this phone has memory card slot ?",
#   "Does this phone have a face recognittion technology ?",
#   "What is the screen size ?",
#   "Does this phone has dual camera ?",
#   "How is the battery life ?",
#   "Is the screen size good enough ?",
#   "What is the body material of the phone ?"
#   ]
# ir.predict(cbm_obj,questions)

# cbm_obj = {'C':'Mobiles',
#            'B' : 'Samsung',
#            'M':'S9 Plus',
#            'industry_code': 1001,
#            'I': 'Cell Phones'}
# questions = [
#   "What is the screen size?",
#   "Is the screen size good enough?",
#   "Does this phone has dual camera?",
#   "How is the battery life?",
#   "Does this phone has fingerprint scanner?",
#   "Does this phone have a face recognittion technology?",
#   "Is front facing camera good enough?",
#   "Is the phone water resistant?",
#   "Does the camera include portrait mode?",
#   "How fast does the phone charge?",
#   "What is the body material of the phone?",
#   "What is the slow motion rate of the camera?",
#   "Does the camera have image stabilization?",
#   "How do you feel about wireless charger?",
#   "Does this phone has memory card slot?",
#   "How do you feel about screen resolution?",
#   "Does this have a protrait mode?"
# ]
# ir.predict(cbm_obj,questions)

# quz = [
# "What is the RAM size?",
# "What is the memory size of the Phone?",
# "Is the memory extensible using external memory card?",
# "What is the screen size of the phone?",
# "What is the screen resolution of the phone?",
# "How many megapixel camera does it have?",
# "Does the phone have dual camera?",
# "Does the camera include portrait mode?",
# "How long does the battery last?",
# "How fast does the phone charge?",
# "Does the phone support wireless charging?",
# "How much does the phone weigh?",
# "Does the phone support multiple colors?",
# "Does it support finger print sensors?",
# "What is the type of processor included in the phone?",
# "Does the phone support dual sim?",
# "Does the phone come with the notch?",
# "What is the battery capacity?",
# "What is the price of the phone?",
# "Does the phone have bezzel?",
# "How many speakers does the phone include?",
# "Is the phone water resistant?",
# "Does it come with qualcomm processor?",
# "How much is the storage of the phone?",
# "What is the body material of the phone?",
# "What is the aperture of the camera ?",
# "What is the focusing system in the camera?",
# "Does the camera have image stabilization?",
# "What is the slow motion rate of the camera?",
# "How do you feel about wireless charger",
# "What is the screen size?",
# "Is the screen size good enough?",
# "Does this phone has dual camera?",
# "How is the battery life?",
# "Does this phone has memory card slot?",
# "Does this phone has fingerprint scanner?",
# "Does this phone have a face recognittion technology?",
# "How do you feel about screen resolution?",
# "Is front facing camera good enough?",
# "Does this have a protrait mode?",
# "Is the phone water resistant?"
# ]

# ir.predict(cbm_obj,quz[0:4])

'''
Request structure:
cbm_obj = {'C':'electronics',
           'B' : 'Xiaomi',
           'M':'Redmi Note 5 Pro'}
questions = ["how do you feel about screen resolution ?"]

Response Structure:
   res= {      
          "data" : [{ "question":"question",
                      "c": "C" ,
                      "b": "B",
                      "m": "M",
                      "s_id": "string",
                      "flat_score_list": [],
                      "normalized_score_list":[],
                      "pairwise_score_list":[],
                      "termToterm_score_list":[],
                      "bilinear_score_list":[],
                      "max_flat_score": 6.73956060303225
                      "max_norm_score": 0.000141472952727899,
                      "L1": null,
                      "L2": null,
                      "L1_L2_combined": null
                    }]
'''
def predict(cbm_obj, questions):
    res = ir.predict(cbm_obj,questions)

    # res = [{
    #     "c" : "mobiles",
    #     "b" : "samsung",
    #     "m" : "galaxy s6",
    #     "i" : "i",
    #     "score" : 0,
    #     "question" : "what can be the question",
    #     "s_id" : "5b5c0201b9c697697816892b",
    #     "ques_score" : 90,
    #     "vid_qua_score" : 40
    # }]
    return res