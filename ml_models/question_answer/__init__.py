#!/usr/bin/env python
""" inference.py : Provides UI for model inference """
__author__ = 'Dharmanath'
__contributers__ = ['Sumanth','Harika']
__copyright__ = 'Copyright 2018, Question & Answer'
__version__ = '1.0.1'
__status__ = 'Production'
__date__ = 'Thu Dec 13 14:13:17 IST 2018'

from .qa_main import rank, Snippet