#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" industry.py : Loads model corresponding to the industry """
__author__ = 'Dharmanath'
__contributers__ = ['Sumanth','Harika']
__copyright__ = 'Copyright 2018, Question & Answer'
__version__ = '1.0.1'
__status__ = 'Production'
__date__ = 'Thu Dec 13 14:13:17 IST 2018'


import tensorflow as tf
from model import Model
from sklearn.feature_extraction.text import CountVectorizer
import pickle
import os

"""
Configurations:
1. Path : abs_path
2. industry_models : {
    'category1' : (category1_model_path,category1_word_dict_path,vocab_size),
    'category2' : (category1_model_path,category1_word_dict_path,vocab_size)
}
3. Top ranked snippets : qa_main.py : model = Sort(topN=5,ascending=False)
"""

class Industry:
    """
    Args : industry_models 
            - Takes a hash of industry id vs list of models
    """
    #abs_path = "/Users/suraj/MyVolume/workspace/django/services/qa_service/ml_models/question_answer/Models"
    abs_path = str(os.getcwd())+"/ml_models/"+"question_answer/Models"

    industry_models = {
        'Cell Phones':(abs_path+'/CHECK.ckpt',abs_path+'/words.pkl',200000,1),
        'Electronics':(abs_path+'/ele_tensorflow_model_10.ckpt',abs_path+'/ele_words.pkl',150000,1),
        'Musical Instruments':(abs_path+'/music.ckpt',abs_path+'/music_words.pkl',26291,1),
        'Beauty and Personal Care':(abs_path+'/beauty_health.ckpt',abs_path+'/bhc_words.pkl',155893,1),
        'General':(abs_path+'/common2.ckpt',abs_path+'/common_words2.pkl',340000,10)
        }
          
    model_sess_list = {}
    model_vocab_list = {}
    model_vSize_list = {}

    def __init__(self):
        """ 
        Args: None
        Returns: None
        Function : Initializes all the models
        """
        for industry_id in self.industry_models.keys():
            try:
                # loading tenserflow models
                path_ = self.industry_models[industry_id][0]
                tf.reset_default_graph()
                model_name = Model(None,savefile=path_,vocab_size=self.industry_models[industry_id][2],k=self.industry_models[industry_id][3])
                model_name.load_model()
                model_index = str(industry_id)+"_"
                self.model_sess_list[model_index] = model_name

                # load vocab
                path_ = self.industry_models[industry_id][1]
                with open(path_, 'rb') as f:
                    v = pickle.load(f)
                self.model_vocab_list[model_index] = v

                # vocab size of the model
                self.model_vSize_list[model_index] = self.industry_models[industry_id][2]
            except Exception as e:
                print e, industry_id

            




    def get_model(self,ind_id):
        """ 
        Args: ind_id : Industry ID
        Returns: Following models corresponds to Industry Id
                    1. Tenserflow model
                    2. TF model
                    3. IDF model
                    4. vocabulary
        Function : Returns the model corrsponds to industry id
        """
        model_index = str(ind_id)+"_"
        #return self.model_sess_list[model_index], self.model_vocab_list[model_index],self.model_vSize_list[model_index]
        return model_index, self.model_vocab_list[model_index],self.model_vSize_list[model_index]
        







