#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" inference.py : Provides UI for model inference """
__author__ = 'Dharmanath'
__contributers__ = ['Sumanth','Harika']
__copyright__ = 'Copyright 2018, Question & Answer'
__version__ = '1.0.1'
__status__ = 'Production'
__date__ = 'Thu Dec 13 14:13:17 IST 2018'


from nltk.stem import SnowballStemmer
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from collections import defaultdict
import numpy as np
from collections import defaultdict
from math import log
tokenizer 	= RegexpTokenizer(r'\w+')
stop 		= set(stopwords.words('english'))
stemmer 	= SnowballStemmer("english")
stopwords_list = stopwords.words('english')

import RAKE.RAKE as rake
from nltk.corpus import stopwords
import nltk
from nltk.stem import WordNetLemmatizer
lematizer = WordNetLemmatizer()
rake_object = rake.Rake(stopwords.words('english'))
import re

class My_Sent:
    def __init__(self,sen,r,id_,idf_cou,snippet_id_,ob_):
        """ 
        Args: 
            1. sen :Sentence
            2. r : Review
            3. id_ : sentence id
            4. snippet_id : Snippet ID
            5. ob_ : Snippet Objectxs

        Returns: None
        Function : Initializes sentence features
        """
        self.snippet_id = snippet_id_
        self.counter = id_
        self.idf_counter = idf_cou
        self.org_sen = sen
        self.sent = [ob_.vocab[stemmer.stem(x)] for x in tokenizer.tokenize(sen) if ((stemmer.stem(x) in ob_.vocab.keys()) and (x not in stopwords_list))]
        self.review = r
        self.tf = None
        self.idf = None

        sen = sen.lower()
        sen = re.sub("[^a-zA-Z ]"," ",sen)
        sen = re.sub(" +"," ",sen)
        self.chuck_sen = self.getPhrases(sen,". ")
        self.chuck_sen = [ob_.vocab[stemmer.stem(x)] for x in tokenizer.tokenize(self.chuck_sen) if ((stemmer.stem(x) in ob_.vocab.keys()) and (x not in stopwords_list))]
        self.chunk_sFeature 	= {}

        # For chunking
        for wordId in self.chuck_sen:
            if wordId in self.chunk_sFeature:
                self.chunk_sFeature[wordId] += 1
            else:
                self.chunk_sFeature[wordId] = np.float64(1)
        
        L 	= self.chunk_sFeature.values()
        norm 	= np.sqrt(np.sum(np.power(L, 2)))

        for k, v  in self.chunk_sFeature.items():
            self.chunk_sFeature[k] = v/norm

        
    
    def getPhrases(self,text,deli):
        keywords = []
        text = text.split(deli)
        for t in text:
            kws = rake_object.run(t)
            keywords.extend(kws)
        return ' '.join(list(set([x[0] for x in keywords])))


class Infer:

    vocab = None
    model = None
    question = None
    avgdl = None

    def __init__(self,vocab,question_,snippets_,V):
        self.sen_lis = list()
        self.PairWiseFeature = {}
        self.item_to_snippet_mapping = {}
        self.sen_lis = list()
        self.vocab = vocab
        self.V = V
        #self.model = model
        self.question = [self.vocab[stemmer.stem(x)] for x in tokenizer.tokenize(question_.lower()) if stemmer.stem(x) in self.vocab.keys()]
        self.snippets = snippets_
        self.N = len(snippets_)
        self.avgdl = 0.0


        idf_counter = 0
        for sn in snippets_:
            snippet_id_ = sn.snippet_id
            self.item_to_snippet_mapping[snippet_id_] = sn
            #sn2 = sn.snippet_text.split(".")
            len_of_sen_th = 5
            sn2 = self.find_sentences(sn.snippet_text,len_of_sen_th)
            sn2 = [x for x in sn2 if len(x)>2]
            counter = 0
            for sentence in sn2:
                ob = My_Sent(sentence,sn,counter,idf_counter,snippet_id_,self)
                counter = counter + 1
                idf_counter = idf_counter + 1
                self.sen_lis.append(ob)

        self.tf_idf_infer()
        self.pre_computations()
        # self.prediction = self.test()
        # self.initialize_values()

    def custom_split(self,data,th):
        pos = [i for i in range(len(data)) if data.startswith(',', i)]
        pos.extend([i for i in range(len(data)) if data.startswith('?', i)])
        pos = sorted(pos)
        #print pos
        prev = 0
        ls = []
        for idx in range(len(pos)):
            temp_data =  data[prev:pos[idx]]
            #print idx,temp_data
            if len(temp_data.split())>th:
                ls.append(temp_data)
                prev = pos[idx]+1
        ls.append(data[prev:])
        ls = [x for x in ls if x!='']
        return ls

    
    def find_sentences(self,data,th):
        data = data.split(".")
        sentences = []
        for x in data:
            sentences.append(self.custom_split(x,th))
        return [item for sublist in sentences for item in sublist]
        

    def initialize_values(self):
        for dict_ in self.prediction:
            sid = dict_['snippet_id']
            sn = self.item_to_snippet_mapping[sid]

            # set flat scores
            ls = sn.get_flat_score_list()
            ls.append(dict_['flat_score'])
            sn.set_flat_score_list(ls)
            # set normalised scores
            ls = sn.get_norm_score_list()
            ls.append(dict_['normaized_score'])
            sn.set_norm_score_list(ls)
            # set pairwise scores
            ls = sn.get_pairwise_score_list()
            ls.append(dict_['pairwise_score'])
            sn.set_pairwise_score_list(ls)
            # set term to term scores
            ls = sn.get_termToterm_score_list()
            ls.append(dict_['termToterm_score'])
            sn.set_termToterm_score_list(ls)
            # set bilinear scores
            ls = sn.get_bilinear_score_list()
            ls.append(dict_['bilinear_score'])
            sn.set_bilinear_score_list(ls)

            sn.max_compute()
            



    
    def calc_LCS(self,str1, str2):

        m 	= len(str1)+1
        n 	= len(str2)+1
        LCS = np.zeros((m,n), dtype= np.int32)
        
        for i in range(1,m):
            for j in range(1,n):
                
                if str1[i-1] == str2[j-1]:
                    LCS[i][j] = LCS[i-1][j-1] + 1
                
                else:
                    LCS[i][j] = max(LCS[i-1][j],LCS[i][j-1])
        return LCS[m-1][n-1]


    def tf_idf_infer(self):
        TF__= defaultdict(int)
        IDF__ = np.zeros(self.V) 
        DF__= defaultdict(int)
        for sent_ in self.sen_lis:
            self.avgdl += len(sent_.sent)
            sent_.tf = defaultdict(int)
            for wordID in sent_.sent:
                TF__[sent_.idf_counter,wordID] +=1
                DF__[wordID,sent_.idf_counter] = 1
                sent_.tf = TF__
        
        for  wordID in range(0,self.V):
            nt = sum([1 for ID,sent in DF__ if wordID == ID])
            if nt != 0:
                IDF__[wordID] = log(self.N+1) - log(nt)
        
        # for sent_ in self.sen_lis:
        #     sent_.idf = IDF__
        self.universal_idf = IDF__

        self.avgdl = (self.avgdl*1.0)/self.N

        


    def pre_computations(self):
        k1 = 1.2
        b = 0.75
        for sent_ in self.sen_lis:
            bm25 = 0.0
            bm25_plus = 0.0
            for word_id in self.question:
                numr = self.universal_idf[word_id] * sent_.tf[sent_.idf_counter,word_id]*(k1 + 1)
                denr = sent_.tf[sent_.idf_counter,word_id] + k1*(1 - b + (b * len(sent_.sent)/self.avgdl))
                bm25 += (numr*1.0)/denr
                bm25_plus += bm25 + self.universal_idf[word_id]
            LCS = self.calc_LCS(self.question,sent_.sent)
            self.PairWiseFeature[(0,sent_.idf_counter)] 	= np.array([[bm25, bm25_plus, LCS]], dtype =np.float64)
        


    def create_QAFeature(self,q):
        qFeature = {}
        for wordId in q:
            if wordId in qFeature:
                qFeature[wordId] += 1
            else:
                qFeature[wordId] = np.float64(1)
        
        return qFeature


    def create_sparse_two(self,qFeature = None):
        answer_list = None
        indices = []
        values  = []
        Y 	= self.sen_lis
        if answer_list is None:
            for i in range(len(Y)):
                chunk_sFeature = self.sen_lis[i].chunk_sFeature
                for v1, c1 in sorted(qFeature.items()):
                    if v1 in chunk_sFeature:
                        indices.append([0, i, v1])
                        values.append(c1 * chunk_sFeature[v1])

            if indices == []:
                indices.append([0,0,0])
                values.append(np.float64(0))

            shape = [1, len(Y), self.V]
            return (np.array(indices), np.array(values), np.array(shape))





    def create_sparse_one(self,qFeature = None):
        indices = []
        values 	= []
        answer_list = None

        if answer_list is None:
            for k, count in sorted(qFeature.items()):
                indices.append([0, k])
                values.append(count)

            if indices == []:
                indices.append([0,0])
                values.append(np.float64(0))
            shape = [1, self.V]
            return (np.array(indices), np.array(values), np.array(shape))



    def test(self):
        indices = []
        values 	= []
        val_ls = list()
        
        for j in range(len(self.sen_lis)):
            for k, count in sorted(self.sen_lis[j].chunk_sFeature.items()):
                indices.append([j,k])
                values.append(count)

        shape 			= [len(self.sen_lis), self.V]
        Review 		= (np.array(indices), np.array(values), np.array(shape))
        Review_I	= (np.array(indices), np.full((len(values)), 1.0/np.sqrt(len(values))), np.array(shape))


        my_Pairwise 	= np.zeros((1, len(self.sen_lis), 3), dtype = np.float64)

        for j in range(len(self.sen_lis)):
            my_Pairwise[0][j] = self.PairWiseFeature[(0, self.sen_lis[j].idf_counter)]

        TermtoTermR = self.create_sparse_two(qFeature = self.create_QAFeature(self.question))

        Question = self.create_sparse_one(qFeature = self.create_QAFeature(self.question))
        Question_I  	= (Question[0], Question[1] if Question[1].size == 1 and Question[1][0] == 0 else np.full((Question[1].size), 1.0/np.sqrt(Question[1].size)), Question[2])
        Review_I 	= (Review[0], np.full((Review[1].size), 1.0/np.sqrt(Review[1].size)), Review[2])
        val_ls.append((my_Pairwise,TermtoTermR,Question_I,Review_I))




        
        for i in range(len(val_ls)):
            topRanked 	= [] 
            pairwise, termtoTermR, question_I, review_I = val_ls[i]
            feed_dict = {
                self.model.pairwise: pairwise,
                        self.model.termTotermR 	: termtoTermR, #
                        self.model.question_I 	: question_I,
                        self.model.review_I 		: review_I
                        }
            Relevance 	= self.model.sess.run(self.model.my_res_, feed_dict = feed_dict)
            pr = Relevance[0][0]
            t2tR = Relevance[3][0]
            blr =  Relevance[4][0]
            Flat_score = Relevance[2][0]
            Relevance 	= Relevance[1][0]
            Relevance   = sorted([(Relevance[i], (i,Flat_score[i],pr[i],t2tR[i],blr[i])) for i in range(len(Relevance))])
		

            for score,ind in Relevance:
                sent = self.sen_lis[ind[0]].org_sen
                #self.sen_lis[ind[0]].snippet_id
                f_score = ind[1]
                temp_dict = {"snippet_id":self.sen_lis[ind[0]].snippet_id,
                "sentence_id":self.sen_lis[ind[0]].counter,
                "flat_score":f_score,
                "normaized_score":score,
                "pairwise_score":ind[2],
                "termToterm_score":ind[3],
                "bilinear_score":ind[4]}
                topRanked.append(temp_dict)

        return topRanked
