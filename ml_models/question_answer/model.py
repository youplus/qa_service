#!/usr/bin/env python
""" inference.py : Provides UI for model inference """
__author__ = 'Dharmanath'
__contributers__ = ['Sumanth','Harika','Naval']
__copyright__ = 'Copyright 2018, Question & Answer'
__version__ = '1.0.1'
__status__ = 'Production'
__date__ = 'Thu Dec 13 14:13:17 IST 2018'

import tensorflow as tf

class Model:

    def __init__(self,corpus,vocab_size,savefile,k=1):
        """ 
        Args: 
            1. corpus : pre-processed data ( Not required for inference )
            2. savefile : save predictions file path ( Not required for inference )
            3. k : ranking dimension 

        Returns: None
        Function : Initializes the model 
        """
        self.PairwiseDim		= 3
        self.rankDim = k
        self.pairwise			= tf.placeholder(dtype = tf.float64, name = 'Pairwise')
        self.question 			= tf.sparse_placeholder(dtype = tf.float64, name = 'Question')
        self.answer 			= tf.sparse_placeholder(dtype = tf.float64, name = 'Answer')
        self.review 			= tf.sparse_placeholder(dtype = tf.float64, name = 'Review')
        self.question_I			= tf.sparse_placeholder(dtype = tf.float64, name = 'Question')
        self.answer_I 			= tf.sparse_placeholder(dtype = tf.float64, name = 'Answer')
        self.review_I 			= tf.sparse_placeholder(dtype = tf.float64, name = 'Review')
        self.termTotermR		= tf.sparse_placeholder(dtype = tf.float64, name = 'Review')
        self.termTotermP		= tf.sparse_placeholder(dtype = tf.float64, name = 'Review')
        self.theta   		= tf.Variable(tf.random_uniform([self.PairwiseDim, 1], dtype = tf.float64), name = 'theta' ) # 3 thetas
        self.V 				= vocab_size
        self.RelvPar 		= tf.Variable(tf.random_uniform([self.V, 1], dtype = tf.float64), name = 'RelvPar') # voab_length * 1
        self.A       		= tf.Variable(tf.random_uniform([self.V, self.rankDim], dtype = tf.float64), name = 'A') # voab_length * k
        self.B		 	= tf.Variable(tf.random_uniform([self.V, self.rankDim], dtype = tf.float64), name = 'B') # vocab_length *
        self.my_res_ = self.calc_relevance(self.pairwise,self.termTotermR,self.question_I,self.review_I)
        self.saver = tf.train.Saver()
        self.savefile= savefile
        
        
        
    def calc_relevance(self,Pairwise,TermtoTermR,Question_I,Review_I):
        """ 
        Args: 
            1. Pairwise : Holds ranking variables - BM25, BM25+, LCS
            2. TermtoTermR : Terms common between question and snippets with corresponding weights
            3. Question_I : weights of words present in question
            4. Review_I : Weights of words present in review

        Returns: 
            1. Pairwise : Holds ranking variables - BM25, BM25+, LCS
            2. Relevance : normalized score 
            3. Flat_score : pre-normalized score
        Function : Initializes the model 
        """
        shape1 		= tf.shape(Pairwise)
        nq 		= shape1[0]
        nr 		= shape1[1]
        pairwise 	= tf.reshape(Pairwise, [-1, self.PairwiseDim])
        pairwise 	= tf.reshape(tf.matmul(pairwise, self.theta), [nq, nr])
        termTotermR 	= tf.sparse_reshape(TermtoTermR, [-1, self.V])
        termTotermR 	= tf.reshape(tf.sparse_tensor_dense_matmul(termTotermR, self.RelvPar), [nq, nr])
        QProj		= tf.sparse_tensor_dense_matmul(Question_I, self.A)
        RProjR		= tf.sparse_tensor_dense_matmul(Review_I, self.B)
        BilinearR	= tf.matmul(QProj, tf.transpose(RProjR))
        Flat_score = pairwise + termTotermR + BilinearR
        Relevance	= tf.nn.softmax(pairwise + termTotermR + BilinearR)
        return pairwise,Relevance,Flat_score,termTotermR,BilinearR

    
    def load_model(self,):
        """ 
        Args: None
        Returns: None
        Function : Loads pre-trained tenserflow model
        """
        sess = tf.Session()
        self.saver.restore(sess, self.savefile)
        self.sess = sess