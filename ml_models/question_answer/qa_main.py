#!/usr/bin/env python
""" inference.py : Provides UI for model inference """
__author__ = 'Dharmanath'
__contributers__ = ['Sumanth','Harika']
__copyright__ = 'Copyright 2018, Question & Answer'
__version__ = '1.0.1'
__status__ = 'Production'
__date__ = 'Thu Dec 13 14:13:17 IST 2018'

from ranked import Documents, Snippet
from industry import Industry
from inference import Infer
from wrapper import Sort as wrapper_sort

import  multiprocessing
from multiprocessing import Pool
mp = multiprocessing.cpu_count()
import time

indus = Industry()
# Please set the model paths in industry.py


def multi_rank(*args):
    """ 
    Args: 
        1. question_ : question
        2. snippets_ : list of snippet objects
            2.1 snippet_id
            2.2 snippet

        3. industry_ : industry code of snippets

    Returns: 
        1. snippets_ : list of updated snippet objects

    Function : Ranks snippets 
    """
    data =  args[0]
    question_ = data[0]
    snippets_ = data[1]
    industry_ = data[2]
    try:
        model_index,vocab,vSize = indus.get_model(industry_)
        ob = Infer(vocab,question_,snippets_,vSize)
        return ob,model_index,question_
    except Exception as e:
        #print "exception from L3 ",e
        return question_,e
    

def rank(data_list):
    try:
        resp = list()
        start_time = time.time()
        pool = Pool(mp-2)
        res = pool.map(multi_rank, data_list)
        
        for ob in res:
            if len(ob)==2:
                resp.append((ob[0],ob[1],None))
                continue

            model = indus.model_sess_list[ob[1]]
            ob[0].model = model
            ob[0].prediction = ob[0].test()
            ob[0].initialize_values()
            sort_model = wrapper_sort(topN=5,ascending=False)
            snippets_ = sort_model.transform(ob[0].snippets)
            snippets_ = sort_model.flat_filter(snippets_,18.0)
            resp.append((ob[2],snippets_))


            # print "===========================***************=========================="
            # print ob[2]
            # for x in snippets_:
            #     print x.get_snippet_text()
            #     print "Norm score :",x.get_max_norm_score()
            #     print "Flat score :",x.get_max_flat_score()
            #     print "-------------------------\n"
        
        pool.close()
        pool.join()
        return resp
    except Exception as e:
        return None


## EXAMPLE 1
# q = "What colors or patterns do the Rothy's Flats come in?"
# a = Snippet(1,"that also the flats round is wide. so if you want a fancy your shoe this is a great option and they come in a few different colors as well now with the new trend of late loafers and mules coming out this fall i wanted to throw in a couple of options for that trend")
# b = Snippet(2,"So you got a variety of colors, patterns, and they do come out with new ones")
# inf = rank(q,[a,b],100)
# inf[0].display_snippet()


## EXAMPLE 2
# question = "How do you feel about screen resolution ?"
# snps = [u'Four thousand milliampere, while the meat ha a smaller three thousand million per battery, the meat support blue to five and the rate note five roman do with butfor point both phone lack nfc but do support dual for gold. Both have five point. Ninety nine inch full hd plus display with eighteen, estonia, aspect ratio and no note the screen on both smartphones are quite reflective, which impact outdoor legibility.',
#  u'There is very little to distinguish between the two phone fingerprint sensor in term of speed and accuracy made out of aluminum porthole feel reassuringly solid in the hand they are a bit too tall to be used comfortably with one handle.',
#  u'You get a usb type c to three point: five monin, the box, which you will need if you want to use your existing headphone. The three point: five mm port is present at the bottom of the red men. Firth ha a usb cord for charging and data transfer, while the red minipro make do with the micro.',
#  u'Show me me two offer a cleaner software package. The promise of timely update a brighter and punchier display and a better rear camera set up if you can live without the headphone jack and the micro sd card slot. The me it is more modern and a better package. Overall, however, the red mineiro still ha some trick up it sleeve.',
#  u'Just like it predecessor, the mi is an angered one, smart phone, with a focus on clean fluid software and timely update this time around samish omitted a few key feature such a the headphone jack and a micro sdcard slot for expandable storage in comparison, read minder retains The legacy port and ha a hybrid doe, trait that let you expand storage by up to fifty six gb to showing me a two is powered by the call: come snapdragon six sixty processor, while the red minories, the snap rightand, six, thirty six.',
#  u'The color temperature on my at is much warmer and the color are slightly more vivid red menores display get brighter. But look likely done in comparison, both smartphones breeze through day to day use and the lever smooth and consistent user experience with quick, apod time and smooth ui animation. However, the mea edge out the red mode, five ro in benchmark. The shame me a is a part of the android one initiative which mean it run: a stock build of android, eight point path, oo with the august security patch. At the time of writing this article with me, you can expect mladen update a well a security patch. The software is fluent and devoid of any third party bloat. There are a few me branded reinstalled application like metrocon, ity, restored and file manager.',
#  u'Let me note five pro in comparison run a heavily skill version of android, eight point, one or me why nine point: five is loaded with useful feature like a one handed mode, a powerful theming engine and duel apps, which let you run a second instance of an App with a second account, however, the experience is not a clean, a talkand with a confused and cluttered setting menu being an android one smartphone. The meat should receive the andreu te soon. There is no information, a of now regarding and android pie plate for the red. We note five proof a is the case with most phone these day, both model support face recognition which work well enough in favorable light, but a hitomi in low, like simply the red mindef, delivers better battery life in our video loop batty test.']
# snps = [Snippet(idx,snps[idx]) for idx in range(len(snps))]
# inf = rank(question,snps,100)
# inf[0].display_snippet()

## Example 3
# question = "How do you feel about screen resolution ?"
# snps = [u'Four thousand milliampere, while the meat ha a smaller three thousand million per battery, the meat support blue to five and the rate note five roman do with butfor point both phone lack nfc but do support dual for gold. Both have five point. Ninety nine inch full hd plus display with eighteen, estonia, aspect ratio and no note the screen on both smartphones are quite reflective, which impact outdoor legibility.',
#  u'There is very little to distinguish between the two phone fingerprint sensor in term of speed and accuracy made out of aluminum porthole feel reassuringly solid in the hand they are a bit too tall to be used comfortably with one handle.',
#  u'You get a usb type c to three point: five monin, the box, which you will need if you want to use your existing headphone. The three point: five mm port is present at the bottom of the red men. Firth ha a usb cord for charging and data transfer, while the red minipro make do with the micro.',
#  u'Show me me two offer a cleaner software package. The promise of timely update a brighter and punchier display and a better rear camera set up if you can live without the headphone jack and the micro sd card slot. The me it is more modern and a better package. Overall, however, the red mineiro still ha some trick up it sleeve.',
#  u'Just like it predecessor, the mi is an angered one, smart phone, with a focus on clean fluid software and timely update this time around samish omitted a few key feature such a the headphone jack and a micro sdcard slot for expandable storage in comparison, read minder retains The legacy port and ha a hybrid doe, trait that let you expand storage by up to fifty six gb to showing me a two is powered by the call: come snapdragon six sixty processor, while the red minories, the snap rightand, six, thirty six.',
#  u'The color temperature on my at is much warmer and the color are slightly more vivid red menores display get brighter. But look likely done in comparison, both smartphones breeze through day to day use and the lever smooth and consistent user experience with quick, apod time and smooth ui animation. However, the mea edge out the red mode, five ro in benchmark. The shame me a is a part of the android one initiative which mean it run: a stock build of android, eight point path, oo with the august security patch. At the time of writing this article with me, you can expect mladen update a well a security patch. The software is fluent and devoid of any third party bloat. There are a few me branded reinstalled application like metrocon, ity, restored and file manager.',
#  u'Let me note five pro in comparison run a heavily skill version of android, eight point, one or me why nine point: five is loaded with useful feature like a one handed mode, a powerful theming engine and duel apps, which let you run a second instance of an App with a second account, however, the experience is not a clean, a talkand with a confused and cluttered setting menu being an android one smartphone. The meat should receive the andreu te soon. There is no information, a of now regarding and android pie plate for the red. We note five proof a is the case with most phone these day, both model support face recognition which work well enough in favorable light, but a hitomi in low, like simply the red mindef, delivers better battery life in our video loop batty test.']
# snps = [Snippet(idx,snps[idx]) for idx in range(len(snps))]
# snps = rank(question,snps,100)

# # Sort wrapper
# from wrapper import Sort
# model = Sort(topN=6,ascending=False)
# snps = model.transform(snps)

# for x in snps:
#     print x.get_snippet_text()
#     print x.get_max_norm_score()
#     print "-------------------------\n"


