#!/usr/bin/env python
""" inference.py : Provides UI for model inference """
__author__ = 'Dharmanath'
__contributers__ = ['Sumanth','Harika']
__copyright__ = 'Copyright 2018, Question & Answer'
__version__ = '1.0.1'
__status__ = 'Production'
__date__ = 'Thu Dec 13 14:13:17 IST 2018'



class Documents:
    question = None
    snippets = None
    industry = None
    soft_scores = None
    flat_scores = None

    def __init__(self,question_,snippets_,industry_):
        """ 
        Args: 
            1. question_ : question
            2. snippets_ : list of snippet objects
            3. industry_ : industry code of snippets

        Returns: 
            1. snippets_ : list of updated snippet objects

        Function : Doc creation
        """
        self.question = question_
        self.snippets = snippets_
        self.industry = industry_




class Snippet:

    def __init__(self,id_,text_,score_x,score_y,score_xy):
        """ 
        Args: 
            1. id_ : question
            2. text_ : list of snippet objects

        Returns: None

        Function : Snippet object creation
        """
        self.snippet_id = id_
        self.snippet_text = text_
        self.flat_score_list = list()
        self.normalized_score_list = list()
        self.pairwise_score_list = list()
        self.termToterm_score_list = list()
        self.bilinear_score_list = list()
        self.max_flat_score = None
        self.max_norm_score = None
        self.L1 = None
        self.L2 = None
        self.L1_mul_L2 = None
        self.max_sentence = None


    def get_max_sentence(self):
        """
        Args : None
        Returns : max score sentence
        Function: Which part of the snippet answers the question
        """
        return self.max_sentence

    def set_max_sentence(self,sen):
        """
        Args : sentence
        Returns : none
        Function: Which part of the snippet answers the question
        """
        self.max_sentence = sen
    

    
    def get_l1(self):
        """ 
        Args: None
        Returns: L1 score
        Function : Returns L1 score of a snippet
        """
        return self.L1

    
    def get_l2(self):
        """ 
        Args: None
        Returns: L2 score
        Function : Returns L2 score of a snippet
        """
        return self.L2

    def get_l1_l2(self):
        """ 
        Args: None
        Returns: L1 * L2 score
        Function : Returns L1 * L2 score of a snippet
        """
        return self.L1_mul_L2


    def get_snippet_text(self):
        """ 
        Args: None
        Returns: snipped text
        Function : Returns snippet text of a snippet
        """
        return self.snippet_text


    def get_snippet_id(self):
        """ 
        Args: None
        Returns: snipped id
        Function : Returns snippet id of a snippet
        """
        return self.snippet_id   

    def get_max_norm_score(self):
        """ 
        Args: None
        Returns: max normalised score
        Function : Returns max normalised score of a snippet
        """
        return self.max_norm_score  

    def get_max_flat_score(self):
        """ 
        Args: None
        Returns: max flat score
        Function : Returns max flat score of a snippet
        """
        return self.max_flat_score        


    def get_flat_score_list(self):
        """ 
        Args: None
        Returns: pre-normalized scores
        Function : Returns pre normalized scores of a snippet
        """
        return self.flat_score_list
    

    def set_flat_score_list(self,ls):
        """ 
        Args: ls : list of pre-normalized scores
        Returns: None
        Function : sets pre-normalized scores to corresponding a snippet
        """
        self.flat_score_list = ls


    def get_pairwise_score_list(self):
        """ 
        Args: None
        Returns: pairwise scores
        Function : Returns pairwise scores of a snippet
        """
        return self.pairwise_score_list
    

    def set_pairwise_score_list(self,ls):
        """ 
        Args: ls : list of pairwise scores
        Returns: None
        Function : sets pairwise scores to corresponding a snippet
        """
        self.pairwise_score_list = ls

    def get_termToterm_score_list(self):
        """ 
        Args: None
        Returns: term to term scores
        Function : Returns term to term scores of a snippet
        """
        return self.termToterm_score_list

    def set_termToterm_score_list(self,ls):
        """ 
        Args: ls : list of term to term scores
        Returns: None
        Function : sets term to term scores to corresponding a snippet
        """
        self.termToterm_score_list = ls


    def get_bilinear_score_list(self):
        """ 
        Args: None
        Returns: Bi-linear scores
        Function : Returns  Bi-linear scores of a snippet
        """
        return self.bilinear_score_list

    def set_bilinear_score_list(self,ls):
        """ 
        Args: ls : list of  Bi-linear scores
        Returns: None
        Function : sets  Bi-linear scores to corresponding a snippet
        """
        self.bilinear_score_list = ls


    def get_norm_score_list(self):
        """ 
        Args: None
        Returns: normalized scores
        Function : Returns normalized scores of a snippet
        """
        return self.normalized_score_list

    def set_norm_score_list(self,ls):
        """ 
        Args: ls : list of normalized scores
        Returns: None
        Function : sets normalized scores to corresponding a snippet
        """
        self.normalized_score_list = ls

    def max_compute(self):
        """ 
        Args: None
        Returns: None
        Function : sets max pre-normalized and normalized scores to corresponding a snippet
        """
        self.max_flat_score = max(self.flat_score_list)
        self.max_norm_score = max(self.normalized_score_list)



    def display_snippet(self):
        """ 
        Args: None
        Returns: None
        Function : Displays snippet
        """
        print "snippet id     :",self.snippet_id
        print "snippet_text   :",self.snippet_text
        print "flat_scores    :",self.flat_score_list
        print "norm_scores    :",self.normalized_score_list
        print "pairwise_scores:",self.pairwise_score_list
        print "t2t_scores     :",self.termToterm_score_list
        print "bilinear_scores:",self.bilinear_score_list
        print "max_flat_score :",self.max_flat_score
        print "max_norm_score :",self.max_norm_score



    

