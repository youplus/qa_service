




class Sort:

    def __init__(self,topN = 5, ascending = False):
        #print "Wrapper is still under development"
        self.topN = topN
        self.ascending = ascending
    
    def transform(self,ls):
        if self.ascending==True:
            return [x['snippet'] for x in sorted([{"score":x.get_max_norm_score(),"snippet":x} for x in ls], key=lambda k: k['score'])][0:self.topN]
        else:
            return [x['snippet'] for x in sorted([{"score":x.get_max_norm_score(),"snippet":x} for x in ls], key=lambda k: k['score'])[::-1]][0:self.topN]


    def flat_filter(self,ls,flat_threshold):
        return [x for x in ls if x.get_max_flat_score()>flat_threshold]





    
