import json
import os
import qa_service
from django.conf import settings

global configuration
configuration = None

def init():
    try:
        global configuration
        config_file = os.path.dirname(qa_service.__file__)+"/config/config.json"
        with open(config_file) as f:
            config = json.loads(f.read())
        configuration = config['constants']
    except Exception as e:
        print e
init()