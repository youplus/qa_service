import logging
from config import config

class Logger(object):
    __logger = None

    @classmethod
    def get_logger(cls):
        if cls.__logger is None:
            logger = logging.getLogger()
            logger.setLevel(logging.INFO)
            hdlr = logging.FileHandler(config.configuration['LOG_PATH'])
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            hdlr.setFormatter(formatter)
            logger.addHandler(hdlr)            
            cls.__logger = logger
        return cls.__logger