from pymongo import MongoClient
from config import config

class MongoCon(object):
   __db = None

   @classmethod
   def get_connection(cls):
       global configuration
       if cls.__db is None:
           client = MongoClient(config.configuration['MONGOCLIENT'],
               username=config.configuration['USERNAME'],
               password=config.configuration['PASSWORD'],
               authSource=config.configuration['DB'])    
           #cls.__db = client['youplus_development']
           cls.__db = client[config.configuration['DB']]
       return cls.__db