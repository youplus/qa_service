import schedule
import time
from controllers import questions_n_snippets
from utilities import y_q_tags, engine
from bson.objectid import ObjectId
from qa_service import log

logger = log.Logger()
log = logger.get_logger()

def init():
    log.info("==========================================================")
    log.info("Scheduler initiated for first time mapping")
    questions_n_snippets.map_question_to_snippets()

def re_init():
    log.info("==========================================================")
    log.info("Scheduler initiated for mapping again and again")
    questions_n_snippets.re_map_question_to_snippets()

def main():
    log.info("In Scheduler main()")
    schedule.every(1).minutes.do(init)
    #schedule.every(20).minutes.do(re_init)
    #schedule.every(30).seconds.do(init)
    while True:
        schedule.run_pending()
        time.sleep(5)

if __name__== "__main__":
    log.info( 'in __name__')
    main()
    #init()
