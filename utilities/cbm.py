from qa_service import mongodb
from qa_service.config import config
import pdb
from qa_service import log

logger = log.Logger()
log = logger.get_logger()

mdb = mongodb.MongoCon()
db = mdb.get_connection()
cbm_coll = db[config.configuration['CBM_COLL']]

def get_cbm():
    try:
        #pdb.set_trace()
        log.info('get CBM')
        cbm = cbm_coll.find({"is_ques_n_snipts_mapd":False})
        info = filter(lambda wdgt_cnfg: wdgt_cnfg['c'], cbm)
        return []
    except Exception as e:
        log.error(e)

def get_c_questions(category):
    try:
        #pdb.set_trace()
        log.info('get_c_questions')
        cbm_doc = cbm_coll.find({"c":category, "b": "", "m": "" })
        #cbm_ques = cbm_coll.find({"c" : {"$in" : ["mobiles", "electronics"]}, "b": "", "m": "" })
        doc = filter(lambda cbm: cbm['c'], cbm_doc)
        return doc[0]#['questions']
    except Exception as e:
        log.error(e)