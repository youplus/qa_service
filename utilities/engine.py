from ml_models import main
from utilities import cbm, view_page, y_q_tags, qa_service_queue, question_video_mapping
from qa_service import mongodb
from qa_service.config import config
import pdb
from qa_service import log
from datetime import datetime
from bson.objectid import ObjectId
import copy

logger = log.Logger()
log = logger.get_logger()

mdb = mongodb.MongoCon()
db = mdb.get_connection()
qu_mpd_snipts_coll = db[config.configuration['QUES_MAPPED_SNIPP_COLL']]
mongo_users_coll = db[config.configuration['MONGO_USERS_COLL']]

# data = {
#    u'5c64a4af213ae9718123a424':[
#       {
#          'qs_c':u'Mobiles',
#          'q_tags_id':ObjectId('5c64a4af213ae9718123a424'),
#          'qs_b':u'Apple',
#          'qs_is_selected':False,
#          'qs_i':u'Cell Phones',
#          'qs_m':u'iPhone X',
#          'qs_termToterm_score_list':[
#             0.0,
#             0.0,
#             0.0,
#             0.0,
#             6.758732828344705
#          ],
#          'qs_L1':None,
#          'question':u'What is the slow motion rate of the camera?',
#          'qs_bilinear_score_list':[
#             -0.5575786011715135,
#             0.8155656483885394,
#             0.9316185975507235,
#             1.1769109219455256,
#             0.011880198629703527
#          ],
#          'qs_L1_L2_combined':None,
#          'qs_is_corrected':False,
#          'qs_max_flat_score':22.884820605479256,
#          'qs_max_norm_score':0.9999961586295474,
#          'qs_pairwise_score_list':[
#             -0.6827077037103714,
#             -0.6827077037103714,
#             -0.6827077037103714,
#             -0.6827077037103714,
#             16.114207578504846
#          ],
#          'qs_L2':None,
#          'qs_flat_score_list':[
#             -1.2402863048818848,
#             0.13285794467816803,
#             0.24891089384035214,
#             0.49420321823515423,
#             22.884820605479256
#          ],
#          'qs_id':ObjectId('5c6ad02b3d139c4c88ea6e08'),
#          'qs_normalized_score_list':[
#             3.3311755882464296e-11,
#             1.315062808703258e-10,
#             1.476888303697769e-10,
#             1.8874556427187138e-10,
#             0.9999961586295474
#          ]
#       }
#    ],
#    u'5c64a1a5213ae9718123a3e5':[
#       {
#          'q_tags_id':ObjectId('5c64a1a5213ae9718123a3e5'),
#          'qs_c':u'Mobiles',
#          'qs_b':u'Apple',
#          'qs_is_selected':False,
#          'qs_i':u'Cell Phones',
#          'qs_m':u'iPhone X',
#          'qs_termToterm_score_list':[
#             0.0,
#             0.0,
#             0.0,
#             0.0,
#             0.0,
#             0.0,
#             0.0,
#             4.819273226337643
#          ],
#          'qs_L1':None,
#          'question':u'How to unlock the phone?',
#          'qs_bilinear_score_list':[
#             -1.3725520281666648,
#             -0.4312599174865373,
#             -0.05375783277431207,
#             -0.006294641926087529,
#             0.6854668943626311,
#             1.118796541744748,
#             2.7565670390929524,
#             3.092468026841474
#          ],
#          'qs_L1_L2_combined':None,
#          'qs_is_corrected':False,
#          'qs_max_flat_score':18.35627818884366,
#          'qs_max_norm_score':0.007493465210647256,
#          'qs_pairwise_score_list':[
#             -0.11555488672745706,
#             -0.11555488672745706,
#             -0.11555488672745706,
#             -0.11555488672745706,
#             -0.11555488672745706,
#             -0.11555488672745706,
#             -0.11555488672745706,
#             10.444536935664544
#          ],
#          'qs_L2':None,
#          'qs_flat_score_list':[
#             -1.488106914894122,
#             -0.5468148042139944,
#             -0.16931271950176913,
#             -0.12184952865354459,
#             0.569912007635174,
#             1.0032416550172911,
#             2.6410121523654952,
#             18.35627818884366
#          ],
#          'qs_id':ObjectId('5c6ad02b3d139c4c88ea6e0d'),
#          'qs_normalized_score_list':[
#             1.804578372223278e-11,
#             4.625660105913101e-11,
#             6.747156595975245e-11,
#             7.075119698791883e-11,
#             1.4130645777454772e-10,
#             2.1794890151816503e-10,
#             1.1210613348211494e-09,
#             0.007493465210647256
#          ]
#       }
#    ]
# }

#res = {"status":True, "data" : data, "message":"done", "error":""}

def map_ques_and_snippets(page_info):
    try:
        log.info('mapping ques and snippets for: '+str(page_info))
        
        # ------- centralised log ------
        log.info("["+str(config.configuration['ENV'])+"]"+" Started question and answer service for page with id " +str(page_info["_id"])+" of category: "+str(page_info["c"])+", brand: "+str(page_info["b"])+", model: "+str(page_info["m"]))

        qa_service_queue.update_queue_status(page_info, {"status":"in_p"})
        #view_page.update_page_status(page_info, {"qns_status":"in_process"})
                                                    ###later can modify the query/ function to get questions to specific C & B & M. 
        cbm_obj = cbm.get_c_questions(page_info['c'])   ###For Now, taking question from only matching category
        modl_ques_parm = [q['ques'] for q in cbm_obj['questions'] if q['use_in_qa_mdl'] == True ] #For Now sending list of questions(["ques1", "ques2"]) to QandA Model, In future we may want to send list of objects([{'ques':'ques1','created_at':'date'}])
        ###request structure for qa model:
        ### {"c": "", "b": "", "m": "", "questions":[]}
        modl_cbm_parm = {"C": page_info['c'], "B": page_info['b'], "M": page_info['m'], "I": cbm_obj['i'], "industry_code": cbm_obj['industry_code']}
        log.info('Request parameters for qa model. CBM: '+str(modl_cbm_parm)+". with "+str(len(modl_ques_parm))+" Qustions")
        log.info('Questions: '+str(modl_ques_parm))
        res = main.predict(modl_cbm_parm, modl_ques_parm)
        #res = {'status': True, 'data': {}}
        if res['status']:
            if not res['data']:
                log.info('for the '+str(len(modl_ques_parm))+' questions. Model given'+str(len(res['data']))+' snipet ids due to lack of videos.')
                #val_to_updt = {"qns_status": "less_videos", "is_qns_mapd": False, "qns_count": 0}
                queue_st = {"status": "less_videos", "qns_count": 0}
            else:
                qns_data = res['data']
                qns_data_cpy = copy.deepcopy(res['data'])
                log.info('Model given '+str(len(res['data']))+' snipet ids for '+str(len(modl_ques_parm))+' questions')
                st1 = y_q_tags.handle_snippets_n_ques(qns_data)
                st2 = question_video_mapping.handle_snippets_n_ques(qns_data_cpy)
                #val_to_updt = {"qns_status": "done", "is_qns_mapd": True, "qns_count": len(res['data'])}
                queue_st = {"status": "done", "qns_count": len(res['data'])}
                log.info('questions and snippets created successfully')
        else:
            log.info('something went wrong with the model. Model response')
            log.info(res)
            #val_to_updt = {"qns_status": res['message'], "is_qns_mapd": False, "qns_count": 0}
            queue_st = {"status": res['message'], "qns_count": 0}
        #view_page.update_page_status(page_info, val_to_updt)
        qa_service_queue.update_queue_status(page_info, queue_st)
        log.info('------------------------------------------------')

        # ------ centralised log ------
        log.info("["+str(config.configuration['ENV'])+"]"+" Completed question and answer service found " +str(len(res['data']))+" snippets for page with id " +str(page_info["_id"])+" of category: "+str(page_info["c"])+", brand: "+str(page_info["b"])+", model: "+str(page_info["m"]))

    except Exception as e:
        #val_to_updt = {"qns_status": "service_failed", "is_qns_mapd": False, "qns_count": 0}
        queue_st = {"status": "service_failed", "qns_count": 0}
        #view_page.update_page_status(page_info, val_to_updt)
        qa_service_queue.update_queue_status(page_info, queue_st)
        log.info("Went to Exception: ")
        log.error(e)
