from qa_service import mongodb
from qa_service.config import config
import pdb
from qa_service import log
from bson.objectid import ObjectId
from datetime import datetime

logger = log.Logger()
log = logger.get_logger()

mdb = mongodb.MongoCon()
db = mdb.get_connection()
qa_service_q_coll = db[config.configuration['QA_SERVICE_QUEUE']]

def get_page():
    try:
        log.info('get page info through API')
        page_info = list(qa_service_q_coll.aggregate([
                            {"$match":{"status":"ytp"}
                            },
                            {  
                            "$sort":{  
                                "created_at":1
                            }
                            },
                            {"$limit":1}
                        ]))

        return page_info
    except Exception as e:
        log.error(e)

#key_val: {"status":"in_p"}
def update_queue_status(page_info, key_val):
    try:
        st = qa_service_q_coll.update({
                                    "_id":page_info['_id']
                                    },
                                    {"$set":key_val}
                                    )

        log.info('Updated qa_service_queue collection with : '+str(key_val))
        log.info('Status code is: '+str(st))
    except Exception as e:
        log.error(e)

def create(page_info):
    try:
        st = qa_service_q_coll.insert({
            "widget_id": page_info["_id"],
            "widget_url": page_info["widget_url"],
            "widget_config_order_number": page_info["widget_config_order_number"],
            "site_id": page_info["site_id"],
            "user_id": page_info["user_id"],
            "c": page_info["c"],
            "b": page_info["b"],
            "m": page_info["m"],
            "status": "ytp",
            "qns_count": 0,
            "created_at": datetime.now(),
            "updated_at": datetime.now()
        })
        log.info('Inserted qa_service_queue collection with : '+str(page_info))
        log.info('Status code is: '+str(st))
    except Exception as e:
        log.error(e)



