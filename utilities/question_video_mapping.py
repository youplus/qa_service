from qa_service import mongodb
from qa_service.config import config
from utilities import y_q_tags
import pdb
from qa_service import log
from bson.objectid import ObjectId

logger = log.Logger()
log = logger.get_logger()

mdb = mongodb.MongoCon()
db = mdb.get_connection()
question_video_mapping_coll = db[config.configuration['QUESTION_VIDEO_MAPPING']]

def handle_snippets_n_ques(snip_n_ques_data):
    try:
        log.info('handling question to snippets in question_video_mapping collection')
        ques_vid_list=[]
        t=[ques_vid_list.extend(v) for k,v in snip_n_ques_data.items()]

        bulk = question_video_mapping_coll.initialize_unordered_bulk_op()
        counter = 0

        for ques_vid in ques_vid_list:
            count=question_video_mapping_coll.find({'q_tags_id':ques_vid['q_tags_id'],
                                                    'qs_c':ques_vid['qs_c'],
                                                    'qs_b':ques_vid['qs_b'],
                                                    'qs_m':ques_vid['qs_m'],
                                                    'question':ques_vid['question']
                                                    }).limit(1).count()
            if count==0:
                q_tags_data=y_q_tags.get_y_q_tag(ques_vid['q_tags_id'])
                ques_vid_data = ques_vid.copy()
                ques_vid_data.update(q_tags_data[0])
                result = ques_vid_data.pop("_id", None)
                bulk.insert(ques_vid_data)
            counter += 1

            if (counter % 500 == 0):
                st = bulk.execute()
                #st = {'nModified': 0, 'nUpserted': 0, 'nMatched': 0, 'writeErrors': [], 'upserted': [], 'writeConcernErrors': [], 'nRemoved': 0, 'nInserted': 1}
                log.info("Bulk Executed in a batch of 500 documents/ snippets")
                bulk = question_video_mapping_coll.initialize_unordered_bulk_op()

        if (counter % 500 != 0):
            st = bulk.execute()
            log.info("Bulk Execution of snippet and question mapping data to question_video_mapping collection is successfull")

        return True
    except Exception as e:
        log.error(e)