from qa_service import mongodb
from qa_service.config import config
from bson.objectid import ObjectId
import pdb
from qa_service import log

logger = log.Logger()
log = logger.get_logger()

mdb = mongodb.MongoCon()
db = mdb.get_connection()
mongo_users_coll = db[config.configuration['MONGO_USERS_COLL']]


def get_page_info():
    try:
        log.info('get page info')
        widget_configs = mongo_users_coll.aggregate([
                    {"$unwind":"$sites"},
                    {"$unwind":"$sites.widget_config"},
                    {"$match":{ 
                                    "sites.widget_config.is_cbm_corrected": True ,
                                    "sites.widget_config.c" : { "$ne": "" },
                                    "$and":[
                                        {"sites.widget_config.qns_status" : { "$ne": "in_process" }},
                                        {"sites.widget_config.qns_status" : { "$ne": "service_failed" }},
                                        #{"sites.widget_config.is_qns_mapd_tst" : False },
                                        {"sites.widget_config.qns_status" : { "$ne": "model_failed" }},
                                        {"sites.widget_config.qns_status" : { "$ne": "less_videos" }}                                      
                                     ],
                                    "$or":[
                                         {"sites.widget_config.is_qns_mapd": { "$exists": False}},
                                         {"sites.widget_config.is_qns_mapd" : False}
                                    ],
                                    "$or":[
                                            {"sites.widget_config.qns_count": { "$exists": False}},
                                            {"sites.widget_config.qns_count" : 0 }
                                    ],
                             }
                    },
                    {"$group":{
                        "_id": "$sites.widget_config._id",
                        "site_id": {"$first": "$sites._id"},
                        "user_id": {"$first": "$_id"},
                        "site_order_number": {"$first":"$sites.site_order_number"},
                        "widget_url": {"$first":"$sites.widget_config.widget_url"},
                        "is_cbm_assigned": {"$first":"$sites.widget_config.is_cbm_assigned"},
                        "c": {"$first":"$sites.widget_config.c"},
                        "b": {"$first":"$sites.widget_config.b"},
                        "m": {"$first":"$sites.widget_config.m"},
                        "product_info": {"$first":"$sites.widget_config.product_info"},
                        "is_cbm_corrected": {"$first":"$sites.widget_config.is_cbm_corrected"},
                        "widget_config_order_number": {"$first":"$sites.widget_config.widget_config_order_number"}
                        }
                     },
                    {"$project" :  { "_id" : 1, "site_id": 1, "user_id":1, "site_order_number" : 1, "base_url" : 1,"widget_url":1,
                                    "is_cbm_assigned":1, "c": 1, "b":1, "m":1, "product_info": 1, "is_cbm_corrected":1, "widget_config_order_number":1}
                    },
                    #{"$skip":1},
                    {"$limit":1}
            ])
        
        page_info = []
        #pdb.set_trace()
        page_info = filter(lambda wdgt_cnfg: wdgt_cnfg['c'], widget_configs)
        return page_info
    except Exception as e:
        log.error(e)

def get_less_videos_page():
    try:
        log.info('get page info')
        widget_configs = mongo_users_coll.aggregate([
                    {"$unwind":"$sites"},
                    {"$unwind":"$sites.widget_config"},
                    {"$match":{ 
                                    "sites.widget_config.is_cbm_corrected": True ,
                                    "$and":[
                                        {"sites.widget_config.qns_status" : { "$ne": "in_process" }},
                                        {"sites.widget_config.qns_status" : { "$ne": "service_failed" }},
                                        {"sites.widget_config.qns_status" : "less_videos" },
                                        {"sites.widget_config.qns_status" : { "$ne": "model_failed" }}                                       
                                     ],
                                    "$or":[
                                         {"sites.widget_config.is_qns_mapd": { "$exists": False}},
                                         {"sites.widget_config.is_qns_mapd" : False}
                                    ],
                                    "$or":[
                                            {"sites.widget_config.qns_count": { "$exists": False}},
                                            {"sites.widget_config.qns_count" : 0 }
                                    ],
                             }
                    },
                    {"$group":{
                        "_id": "$sites.widget_config._id",
                        "site_id": {"$first": "$sites._id"},
                        "user_id": {"$first": "$_id"},
                        "site_order_number": {"$first":"$sites.site_order_number"},
                        "widget_url": {"$first":"$sites.widget_config.widget_url"},
                        "is_cbm_assigned": {"$first":"$sites.widget_config.is_cbm_assigned"},
                        "c": {"$first":"$sites.widget_config.c"},
                        "b": {"$first":"$sites.widget_config.b"},
                        "m": {"$first":"$sites.widget_config.m"},
                        "product_info": {"$first":"$sites.widget_config.product_info"},
                        "is_cbm_corrected": {"$first":"$sites.widget_config.is_cbm_corrected"},
                        "widget_config_order_number": {"$first":"$sites.widget_config.widget_config_order_number"}
                        }
                     },
                    {"$project" :  { "_id" : 1, "site_id": 1, "user_id":1, "site_order_number" : 1, "base_url" : 1,"widget_url":1,
                                    "is_cbm_assigned":1, "c": 1, "b":1, "m":1, "product_info": 1, "is_cbm_corrected":1, "widget_config_order_number":1}
                    },
                    #{"$skip":1},
                    {"$limit":1}
            ])
        
        page_info = []
        #pdb.set_trace()
        page_info = filter(lambda wdgt_cnfg: wdgt_cnfg['c'], widget_configs)
        return page_info
    except Exception as e:
        log.error(e)

def get_page(user_id, site_id, widget_id):
    try:
        log.info('get page info through API')
        widget_configs = list(mongo_users_coll.aggregate([
            { "$unwind" : "$sites"},
            { "$unwind" : "$sites.widget_config"},
            { "$match" :
                { "_id" : ObjectId(user_id),
                    "sites._id" : ObjectId(site_id),
                    "sites.widget_config._id" : ObjectId(widget_id)
                }
            },
            { "$group":
                {
                    "_id": "$sites.widget_config._id",
                    "site_id": {"$first": "$sites._id"},
                    "user_id": {"$first": "$_id"},
                    "site_order_number": {"$first":"$sites.site_order_number"},
                    "widget_url": {"$first":"$sites.widget_config.widget_url"},
                    "is_cbm_assigned": {"$first":"$sites.widget_config.is_cbm_assigned"},
                    "c": {"$first":"$sites.widget_config.c"},
                    "b": {"$first":"$sites.widget_config.b"},
                    "m": {"$first":"$sites.widget_config.m"},
                    "product_info": {"$first":"$sites.widget_config.product_info"},
                    "is_cbm_corrected": {"$first":"$sites.widget_config.is_cbm_corrected"},
                    "widget_config_order_number": {"$first":"$sites.widget_config.widget_config_order_number"}
                }
            },
            { "$project" :
                {
                    "_id" : 1, "site_id": 1, "user_id":1, "site_order_number" : 1, "base_url" : 1,"widget_url":1,"is_cbm_assigned":1, "c": 1, "b":1, "m":1, "product_info": 1, "is_cbm_corrected":1, "widget_config_order_number":1
                }
            },
        ]))

        return widget_configs
        # page_info = filter(lambda wdgt_cnfg: wdgt_cnfg['c'], widget_configs)
        # return page_info
    except Exception as e:
        log.error(e)

def update_page_status(page_info, key_val):
    val_to_set = {}
    for key, val in key_val.items():
        val_to_set["sites.$.widget_config."+str(page_info['widget_config_order_number'])+'.'+key] = val
    st = mongo_users_coll.update({
            "_id": page_info['user_id'], 
            "sites._id": page_info['site_id'], 
            "sites.widget_config._id": page_info['_id']
         },
            {"$set": val_to_set}
        )
    log.info('Updated mongo_users collection with : '+str(key_val))
    log.info('Status code is: '+str(st))

