from qa_service import mongodb
from qa_service.config import config
import pdb
from qa_service import log
from bson.objectid import ObjectId

logger = log.Logger()
log = logger.get_logger()

mdb = mongodb.MongoCon()
db = mdb.get_connection()
y_q_tags_coll = db[config.configuration['Y_Q_TAGS_COLL']]


'''
    Author: Suraj
    Date: 25-Jan-2019

    This function handles the data of a Snippet having many Questions. Input data will be having below format.
    'snip_n_ques_data' = {'sid1': [{'question': 'q1', 'is_selected' : False, 'is_corrected' : False, 'max_flat_score' : 15.0094684891407, ...},
                                {'question': 'q2', 'is_selected' : False, 'is_corrected' : False, 'max_flat_score' : 16.0081684891407, ...}
                                ],
                        'sid2': [{'question': 'q1', 'is_selected' : False, 'is_corrected' : False, 'max_flat_score' : 25.0094684891407, ...}
                                ],
                        }
    Below are operation happening:
        1. Initializing a bulk and executing DB update in a bulk operation of the batch of 500.
        2. Loopeing over each snippet and questions.
        3. Sorting the Questions on 'max_flat_score', higher the score higher the relevent
        4. Delete all the previously inserted questions for a snippet by considering "is_selected" : False, 'is_corrected' : False.
        5. Skip inserting a question if it already inserted before. 

'''
def handle_snippets_n_ques(snip_n_ques_data):
    try:
        log.info('handling question to snippets in youplus_q_tags collection')
        bulk = y_q_tags_coll.initialize_unordered_bulk_op()
        counter = 0
        
        for sid, questions in snip_n_ques_data.items():
            ques_srtd =  sorted(questions, key=lambda x: x['qs_max_flat_score'], reverse=True)
            log.info("Snippet Id : "+str(sid))
            ##Removing Questions which are not yet selected and corrected
            # bulk.find({ '_id': ObjectId(sid) }).update(
            #           { '$pull': { 'mpd_questions': { "is_selected" : False, 'is_corrected' : False } } } 
            #          )
            for ques_obj in ques_srtd:
                #bulk.find({ '_id': ObjectId(sid), 'mpd_questions.question': {'$ne': ques_obj["question"]} }).update({ '$addToSet': { 'mpd_questions': ques_obj } })
                ##Pushing question if it doesn't exist before and have a condition to have only 5 elements in an array.
                # bulk.find({'_id': ObjectId(sid), 'mpd_questions.question': {'$ne': ques_obj["question"]} }).update(
                #     { '$push': {
                #             'mpd_questions': {
                #                 '$each': [ ques_obj ],
                #                 '$slice': 5
                #             }
                #         }
                #     }
                #     )
                ##Pushing question if its not exists.
                ques_obj.pop('q_tags_id', None)
                bulk.find({'_id': ObjectId(sid), 'mpd_questions.question': {'$ne': ques_obj["question"]} }).update(
                        { '$push': {'mpd_questions':  ques_obj  } }
                        )
            counter += 1

            if (counter % 500 == 0):
                bulk.execute()
                log.info("Bulk Executed in a batch of 500 documents/ snippets")
                bulk = y_q_tags_coll.initialize_unordered_bulk_op()

        if (counter % 500 != 0):
            bulk.execute()
            log.info("Bulk Execution of snippet and question mapping data to youplus_q_tags collection is successfull")
        return True
    except Exception as e:
        log.info("Exception: in y_q_tags.handle_snippets_n_ques()")
        log.error(e)

def get_y_q_tag(id):
    try:
        y_q_tags=list(y_q_tags_coll.find({'_id': id }))
        return y_q_tags
    except Exception as e:
        log.info("Exception: in y_q_tags.get_y_q_tag()")
        log.error(e)